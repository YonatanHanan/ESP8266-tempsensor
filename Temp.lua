file.remove("ap_list.lua")

pin=7
gpio.mode(pin, gpio.OUTPUT)
gpio.write(pin, gpio.HIGH)

tmr.delay(500)

ds18b20.setup(6)

print("setup done")

file.open("settings.lua", "r")
lines = {}
for i = 0, 4, 1 do
    lines[i] = file.readline()
    lines[i] = lines[i]:gsub("%\n", "")
end
file.close()

minit = lines[2] * 60000000
if(lines[0] == '1') then

    print("Settings Done")
    file.open("counter.lua", "r")
    count = file.read()
    file.close()
    count = tonumber(count) + 1
    file.remove("counter.lua")
    file.open("counter.lua", "w+")
    file.writeline(count)
    file.close()
    print("count" .. count)
    ds18b20.read(
    function(ind,rom,res,temp,tdec,par)
        print(temp)
        if((temp >= tonumber(lines[1])) and (temp<= tonumber(lines[4]))) then
            print("Sleeping :)")

            file.remove("state.lua")
            file.open("state.lua", "w+")
            file.writeline(3)
            file.close()
            node.dsleep(minit, 4)
        else            
            print("Sending email...")
            file.open("bad_temp.lua", "w+")
            file.writeline(temp)  
            file.close()

            file.remove("state.lua")
            file.open("state.lua", "w+")
            file.writeline(4)
            file.close()
                
            node.dsleep(500, 4)
        end
    end,{});
end
