file.open("state.lua", "r")
state = file.read()
file.close()

gpio.mode(5, gpio.INPUT)
level = gpio.read(5)

if(level == 0) then
    level = gpio.read(5)
    tmr.delay(2900000)
    level = gpio.read(5)
    if(level == 0) then
        state = '1\n'
    end
end

    
-- Get AP list
if(state == '1\n') then
    print("Get AP list")
    dofile("wifi_ap.lua")
end

-- Start Settings
if(state == '2\n') then
    print("Start Settings")
    dofile("Create_AP.lua")
end

-- Temp
if(state == '3\n') then
    gpio.mode(4, gpio.OUTPUT)
    gpio.write(4, gpio.HIGH)
    gpio.mode(4,gpio.OUTPUT)    
    
    gpio.write(4, gpio.LOW)
    tmr.delay(4000)
    gpio.write(4, gpio.HIGH)

    print("Temp")
    dofile("Temp.lua")
end

-- Send Data
if(state == '4\n') then
    print("Send Data")
    dofile("Email.lua")
end
